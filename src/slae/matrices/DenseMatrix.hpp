//
// Created by ivan on 25.02.23.
//

#ifndef SLAE_DENSEMATRIX_HPP
#define SLAE_DENSEMATRIX_HPP

#include "types/Vector.hpp"

template <typename T, int M, int N> class DenseMatrix {
  static_assert(N >= 0, "Size should be greater or equal zero or -1");
  static_assert(M >= 0, "Size should be greater or equal zero or -1");
  Vector<T, N * M> data;

public:
  const T &operator()(int i, int j) const;
  T &operator()(int i, int j);

  constexpr std::size_t getRowNum() const { return M; }
  constexpr std::size_t getColumnNum() const { return N; }

  DenseMatrix<T, N, M> transpose() const;

  DenseMatrix<T, M, N> &operator*=(T num);
  DenseMatrix<T, M, N> &operator/=(T num);
  DenseMatrix<T, M, N> &operator+=(const DenseMatrix<T, M, N> &other);
  DenseMatrix<T, M, N> &operator-=(const DenseMatrix<T, M, N> &other);

  DenseMatrix<T, M, N> operator*(T num) const;
  DenseMatrix<T, M, N> operator/(T num) const;
  DenseMatrix<T, M, N> operator+(const DenseMatrix<T, M, N> &other) const;
  DenseMatrix<T, M, N> operator-(const DenseMatrix<T, M, N> &other) const;

  template <int P>
  DenseMatrix<T, M, P> operator*(const DenseMatrix<T, N, P> &other) const;

  Vector<T, M> operator*(const Vector<T, N> &other) const;

  friend DenseMatrix<T, M, N> operator*(T num,
                                        const DenseMatrix<T, M, N> &other) {
    return other * num;
  }
};

template <typename T, int M, int N>
DenseMatrix<T, N, M> DenseMatrix<T, M, N>::transpose() const {
  DenseMatrix<T, N, M> res;
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < N; ++j)
      res(j, i) = data[i * N + j];
  }
  return res;
}


template <typename T, int M, int N>
const T &DenseMatrix<T, M, N>::operator()(int i, int j) const {
  return data[i * N + j];
}

template <typename T, int M, int N>
T &DenseMatrix<T, M, N>::operator()(int i, int j) {
  return data[i * N + j];
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> &DenseMatrix<T, M, N>::operator*=(T num) {
  data *= num;
  return *this;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> &DenseMatrix<T, M, N>::operator/=(T num) {
  data /= num;
  return *this;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> &
DenseMatrix<T, M, N>::operator+=(const DenseMatrix<T, M, N> &other) {
  data += other.data;
  return *this;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> &
DenseMatrix<T, M, N>::operator-=(const DenseMatrix<T, M, N> &other) {
  data -= other.data;
  return *this;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> DenseMatrix<T, M, N>::operator*(T num) const {
  DenseMatrix<T, M, N> res;
  res.data = data * num;
  return res;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N> DenseMatrix<T, M, N>::operator/(T num) const {
  DenseMatrix<T, M, N> res;
  res.data = data / num;
  return res;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N>
DenseMatrix<T, M, N>::operator+(const DenseMatrix<T, M, N> &other) const {
  DenseMatrix<T, M, N> res;
  res.data = data + other.data;
  return res;
}

template <typename T, int M, int N>
DenseMatrix<T, M, N>
DenseMatrix<T, M, N>::operator-(const DenseMatrix<T, M, N> &other) const {
  DenseMatrix<T, M, N> res;
  res.data = data - other.data;
  return res;
}

template <typename T, int M, int N>
template <int P>
DenseMatrix<T, M, P>
DenseMatrix<T, M, N>::operator*(const DenseMatrix<T, N, P> &other) const {
  DenseMatrix<T, M, P> res;
  for (int i = 0; i < M; ++i) {
    for (int j = 0; j < P; ++j) {
      T s = 0;
      for (int k = 0; k < N; ++k) {
        s += data[i * N + k] * other(k, j);
      }
      res(i, j) = s;
    }
  }
  return res;
}

template <typename T, int M, int N>
Vector<T, M> DenseMatrix<T, M, N>::operator*(const Vector<T, N> &other) const {
  Vector<T, M> res;
  for (int i = 0; i < M; ++i) {
    T s = 0;
    for (int k = 0; k < N; ++k) {
      s += data[i * N + k] * other[k];
    }
    res[i] = s;
  }
  return res;
}

template <typename T> class DenseMatrix<T, Dynamic, Dynamic> {
  Vector<T, Dynamic> data;
  std::size_t row_num;
  std::size_t col_num;

public:
  DenseMatrix(std::size_t M, std::size_t N)
      : row_num(M), col_num(N), data(Vector<T, Dynamic>(M * N)){};

  const T &operator()(int i, int j) const;
  T &operator()(int i, int j);

  constexpr std::size_t getRowNum() const { return row_num; }
  constexpr std::size_t getColumnNum() const { return col_num; }

  DenseMatrix<T, Dynamic, Dynamic> transpose() const;

  DenseMatrix<T, Dynamic, Dynamic> &operator*=(T num);
  DenseMatrix<T, Dynamic, Dynamic> &operator/=(T num);
  DenseMatrix<T, Dynamic, Dynamic> &
  operator+=(const DenseMatrix<T, Dynamic, Dynamic> &other);
  DenseMatrix<T, Dynamic, Dynamic> &
  operator-=(const DenseMatrix<T, Dynamic, Dynamic> &other);

  DenseMatrix<T, Dynamic, Dynamic> operator*(T num) const;
  DenseMatrix<T, Dynamic, Dynamic> operator/(T num) const;
  DenseMatrix<T, Dynamic, Dynamic>
  operator+(const DenseMatrix<T, Dynamic, Dynamic> &other) const;
  DenseMatrix<T, Dynamic, Dynamic>
  operator-(const DenseMatrix<T, Dynamic, Dynamic> &other) const;

  DenseMatrix<T, Dynamic, Dynamic>
  operator*(const DenseMatrix<T, Dynamic, Dynamic> &other) const;

  Vector<T, Dynamic> operator*(const Vector<T, Dynamic> &other) const;

  friend DenseMatrix<T, Dynamic, Dynamic>
  operator*(T num, const DenseMatrix<T, Dynamic, Dynamic> &other) {
    return other * num;
  }
};

template <typename T>
DenseMatrix<T, Dynamic, Dynamic>
DenseMatrix<T, Dynamic, Dynamic>::transpose() const {
  DenseMatrix<T, Dynamic, Dynamic> res(col_num, row_num);
  for (int i = 0; i < row_num; ++i) {
    for (int j = 0; j < col_num; ++j)
      res(j, i) = data[i * col_num + j];
  }
  return res;
}

template <typename T>
const T &DenseMatrix<T, Dynamic, Dynamic>::operator()(int i, int j) const {
  return data[i * col_num + j];
}

template <typename T>
T &DenseMatrix<T, Dynamic, Dynamic>::operator()(int i, int j) {
  return data[i * col_num + j];
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> &
DenseMatrix<T, Dynamic, Dynamic>::operator*=(T num) {
  data *= num;
  return *this;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> &
DenseMatrix<T, Dynamic, Dynamic>::operator/=(T num) {
  data /= num;
  return *this;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> &DenseMatrix<T, Dynamic, Dynamic>::operator+=(
    const DenseMatrix<T, Dynamic, Dynamic> &other) {
  data += other.data;
  return *this;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> &DenseMatrix<T, Dynamic, Dynamic>::operator-=(
    const DenseMatrix<T, Dynamic, Dynamic> &other) {
  data -= other.data;
  return *this;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic>
DenseMatrix<T, Dynamic, Dynamic>::operator*(T num) const {
  DenseMatrix<T, Dynamic, Dynamic> res(row_num, col_num);
  res.data = data * num;
  return res;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic>
DenseMatrix<T, Dynamic, Dynamic>::operator/(T num) const {
  DenseMatrix<T, Dynamic, Dynamic> res(row_num, col_num);
  res.data = data / num;
  return res;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> DenseMatrix<T, Dynamic, Dynamic>::operator+(
    const DenseMatrix<T, Dynamic, Dynamic> &other) const {
  DenseMatrix<T, Dynamic, Dynamic> res(row_num, col_num);
  res.data = data + other.data;
  return res;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> DenseMatrix<T, Dynamic, Dynamic>::operator-(
    const DenseMatrix<T, Dynamic, Dynamic> &other) const {
  DenseMatrix<T, Dynamic, Dynamic> res(row_num, col_num);
  res.data = data - other.data;
  return res;
}

template <typename T>
DenseMatrix<T, Dynamic, Dynamic> DenseMatrix<T, Dynamic, Dynamic>::operator*(
    const DenseMatrix<T, Dynamic, Dynamic> &other) const {
  DenseMatrix<T, Dynamic, Dynamic> res(row_num, other.col_num);
  for (int i = 0; i < row_num; ++i) {
    for (int j = 0; j < other.col_num; ++j) {
      T s = 0;
      for (int k = 0; k < col_num; ++k) {
        s += data[i * col_num + k] * other(k, j);
      }
      res(i, j) = s;
    }
  }
  return res;
}

template <typename T>
Vector<T, Dynamic> DenseMatrix<T, Dynamic, Dynamic>::operator*(
    const Vector<T, Dynamic> &other) const {
  Vector<T, Dynamic> res(row_num);
  for (int i = 0; i < row_num; ++i) {
    T s = 0;
    for (int k = 0; k < col_num; ++k) {
      s += data[i * col_num + k] * other[k];
    }
    res[i] = s;
  }
  return res;
}
#endif // SLAE_DENSEMATRIX_HPP
