//
// Created by ivan on 07.02.23.
//

#ifndef SLAE_TRIDIAGONAL_HPP
#define SLAE_TRIDIAGONAL_HPP

#include "types/Vector.hpp"

/**fixme**/
//add new constructors - init_list, Dynamic by size;
template <typename T> struct ThreeDiagonalElement {
  T left   = 0;
  T middle = 0;
  T right  = 0;
};

template <typename T, int N> class ThreeDiagonal {
  static_assert(N > 0, "Size is greater than zero or eq. -1 for dynamic");
  Vector<ThreeDiagonalElement<T>, N> data_;

public:
  constexpr ThreeDiagonal(const ThreeDiagonal<T, N> &t_diag);
  constexpr ThreeDiagonal(const Vector<T, N - 1> &lower,
                          const Vector<T, N> &middle,
                          const Vector<T, N - 1> &upper);
  constexpr int getSize() const { return N; }

  const T operator()(int i, int j) const;
  T operator()(int i, int j);
  const ThreeDiagonalElement<T> &operator[](unsigned int i) const {
    return data_[i];
  }
  ThreeDiagonalElement<T> &operator[](unsigned int i) { return data_[i]; }
};

template <typename T, int N>
constexpr ThreeDiagonal<T, N>::ThreeDiagonal(const ThreeDiagonal<T, N> &t_diag)
    : data_(t_diag.data_) {}

template <typename T, int N>
constexpr ThreeDiagonal<T, N>::ThreeDiagonal(const Vector<T, N - 1> &lower,
                                             const Vector<T, N> &middle,
                                             const Vector<T, N - 1> &upper) {
  if (N == 1) {
    data_[0].middle = middle[0];
  } else {
    data_[0].middle = middle[0];
    data_[0].right = upper[0];
    for (auto i = 1; i < N - 1; ++i) {
      data_[i].left = lower[i - 1];
      data_[i].middle = middle[i];
      data_[i].right = upper[i];
    }
    data_[N - 1].left = lower[N - 2];
    data_[N - 1].middle = middle[N - 1];
  }
}

template <typename T, int N>
const T ThreeDiagonal<T, N>::operator()(int i, int j) const {
  switch (i - j) {
  case -1:
    return data_[i].right;
  case 0:
    return data_[i].middle;
  case 1:
    return data_[i].left;
  }
  return 0;
}

template <typename T, int N> T ThreeDiagonal<T, N>::operator()(int i, int j) {
  switch (i - j) {
  case -1:
    return data_[i].right;
  case 0:
    return data_[i].middle;
  case 1:
    return data_[i].left;
  }
  return 0;
}

template <typename T> class ThreeDiagonal<T, Dynamic> {
  Vector<ThreeDiagonalElement<T>, Dynamic> data_;
  int size_;

public:
  constexpr ThreeDiagonal(const int &n);
  constexpr ThreeDiagonal(const Vector<T, Dynamic> &lower,
                          const Vector<T, Dynamic> &middle,
                          const Vector<T, Dynamic> &upper);
  constexpr int getSize() const { return size_; }

  const T operator()(int i, int j) const;
  T operator()(int i, int j);
  const ThreeDiagonalElement<T> &operator[](unsigned int i) const {
    return data_[i];
  }
  ThreeDiagonalElement<T> &operator[](unsigned int i) { return data_[i]; }
};

template <typename T>
constexpr ThreeDiagonal<T, Dynamic>::ThreeDiagonal(
    const Vector<T, Dynamic> &lower, const Vector<T, Dynamic> &middle,
    const Vector<T, Dynamic> &upper)
    : data_(Vector<ThreeDiagonalElement<T>, Dynamic>(middle.getSize())),
      size_(middle.getSize()) {
  if (size_ == 1) {
    data_[0].middle = middle[0];
  } else {
    data_[0].middle = middle[0];
    data_[0].right = upper[0];
    for (auto i = 1; i < size_ - 1; ++i) {
      data_[i].left = lower[i - 1];
      data_[i].middle = middle[i];
      data_[i].right = upper[i];
    }
    data_[size_ - 1].left = lower[size_ - 2];
    data_[size_ - 1].middle = middle[size_ - 1];
  }
}

template <typename T>
const T ThreeDiagonal<T, Dynamic>::operator()(int i, int j) const {
  switch (i - j) {
  case -1:
    return data_[i].right;
  case 0:
    return data_[i].middle;
  case 1:
    return data_[i].left;
  }
  return 0;
}

template <typename T> T ThreeDiagonal<T, Dynamic>::operator()(int i, int j) {
  switch (i - j) {
  case -1:
    return data_[i].right;
  case 0:
    return data_[i].middle;
  case 1:
    return data_[i].left;
  }
  return 0;
}

#endif // SLAE_TRIDIAGONAL_HPP
