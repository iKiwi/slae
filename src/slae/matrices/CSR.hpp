#include "types/Vector.hpp"
#include <initializer_list>
#include <set>

#ifndef SLAE_CSR_HPP
#define SLAE_CSR_HPP

template <typename T> struct matrix_element {
  std::size_t i; // Индекс строки [0, M]
  std::size_t j; // Индекс столбца [0, N]
  T value;
  bool operator<(const matrix_element &r) const {
    return i < r.i || (i == r.i && j < r.j);
  }
};

template <typename T> struct v_c {
  T v;
  size_t c;
};

template <typename T> class CSR {
  std::size_t row_num;
  std::size_t col_num;
  std::vector<v_c<T>> val_col;
  std::vector<T> rows;

public:
  constexpr CSR(std::size_t row_, std::size_t col_,
                const std::set<matrix_element<T>> &elements);
  constexpr CSR(std::size_t N, const std::vector<v_c<T>> &v_c,
                const std::vector<T> &r)
      : val_col(v_c), rows(r), row_num(r.size() - 1), col_num(N) {}
  [[nodiscard]] constexpr std::size_t getRowNum() const noexcept {
    return row_num;
  }
  [[nodiscard]] constexpr std::size_t getColumnNum() const noexcept {
    return col_num;
  }

  const std::vector<v_c<T>> &getValCol() const noexcept { return val_col; }
  const std::vector<T> &getRows() const noexcept { return rows; }

  T operator()(int i, int j) const noexcept;
  Vector<T, -1> operator*(const Vector<T, -1> &vec) const noexcept;
};

template <typename T>
constexpr CSR<T>::CSR(size_t row_, size_t col_,
                      const std::set<matrix_element<T>> &elements)
    : row_num(row_), col_num(col_), val_col(std::vector<v_c<T>>(elements.size())), rows(std::vector<T>(row_+1)){
  std::size_t k = 0;
  for (auto &el : elements) {
    val_col[k] = {el.value, el.j};
    rows[el.i + 1]++;
    ++k;
  }
  for (std::size_t m = 1; m < rows.size(); ++m)
    rows[m] += rows[m - 1];
}

template <typename T>
Vector<T, -1> CSR<T>::operator*(const Vector<T, -1> &vec) const noexcept {
  Vector<T, -1> res(row_num);
  for (int i = 0; i < row_num; ++i) {
    for (int j = rows[i]; j < rows[i + 1]; ++j) {
      res[i] += val_col[j].v * vec[val_col[j].c];
    }
  }
  return res;
}

template <typename T> T CSR<T>::operator()(int i, int j) const noexcept {
  for (int k = rows[i]; k < rows[i + 1]; ++k) {
    if (val_col[k].c == j) {

      return val_col[k].v;
    }
  }
  return 0;
}
#endif // SLAE_CSR_HPP
