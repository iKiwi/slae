//
// Created by is on 7/5/2022.
//

#ifndef VECTOR_H
#define VECTOR_H

#include <array>
#include <cmath>
#include <initializer_list>
#include <vector>

template <typename Type, int N> class Vector {
  static_assert(N >= 0, "Size should be greater or equal zero or -1");
  std::array<Type, N> data_;

public:
  constexpr Vector() = default;
  constexpr Vector(const Vector<Type, N> &other);
  constexpr Vector(std::initializer_list<Type> s);

  const Type &operator[](unsigned int i) const noexcept { return data_[i]; }
  Type &operator[](unsigned int i) noexcept { return data_[i]; }

  Type norm() const noexcept;
  Type maxNorm() const noexcept;
  Type dot(const Vector<Type, N> &other) const noexcept;

  [[nodiscard]] constexpr std::size_t getSize() const noexcept {
    return static_cast<std::size_t>(N);
  }
  bool operator==(const Vector<Type, N> &other) noexcept;

  Vector<Type, N> &operator*=(Type num) noexcept;
  Vector<Type, N> &operator/=(Type num) noexcept;
  Vector<Type, N> &operator+=(const Vector<Type, N> &other) noexcept;
  Vector<Type, N> &operator-=(const Vector<Type, N> &other) noexcept;

  Vector<Type, N> operator*(Type num) const noexcept;
  Vector<Type, N> operator/(Type num) const noexcept;
  Vector<Type, N> operator+(const Vector<Type, N> &other) const noexcept;
  Vector<Type, N> operator-(const Vector<Type, N> &other) const noexcept;

  friend Vector<Type, N> operator*(Type num,
                                   const Vector<Type, N> &vec) noexcept {
    return vec * num;
  }
};

template <typename Type, int N>
constexpr Vector<Type, N>::Vector(const Vector<Type, N> &other)
    : data_(other.data_){};
template <typename Type, int N>
constexpr Vector<Type, N>::Vector(std::initializer_list<Type> s) {
  for (auto i = 0; i < N; i++) {
    data_[i] = *(s.begin() + i);
  }
}

template <typename Type, int N> Type Vector<Type, N>::maxNorm() const noexcept {
  Type p = std::abs(data_[0]);
  for (const auto& elem: data_) {
    if(p<std::abs(elem)){
      p = std::abs(elem);
    }
  }
  return p;
}

template <typename Type, int N> Type Vector<Type, N>::norm() const noexcept {
  Type p = 0;
  for (const auto& elem: data_) {
    p += elem*elem;
  }
  return std::sqrt(p);
}

template <typename Type, int N>
Type Vector<Type, N>::dot(const Vector<Type, N> &other) const noexcept {
  Type dot_pr = 0;
  for (int i = 0; i < N; ++i) {
    dot_pr += data_[i] * other.data_[i];
  }
  return dot_pr;
}

template <typename Type, int N>
bool Vector<Type, N>::operator==(const Vector<Type, N> &other) noexcept {
  for (int i = 0; i < N; i++) {
    if (data_[i] != other.data_[i]) {
      return false;
    }
  }
  return true;
}

template <typename Type, int N>
Vector<Type, N> &Vector<Type, N>::operator*=(Type num) noexcept {
  for (auto &elem : data_) {
    elem *= num;
  }
  return *this;
}

template <typename Type, int N>
Vector<Type, N> &Vector<Type, N>::operator/=(Type num) noexcept {
  return ((*this) *= (static_cast<Type>(1) / num));
}

template <typename Type, int N>
Vector<Type, N> &
Vector<Type, N>::operator+=(const Vector<Type, N> &other) noexcept {
  for (int i = 0; i < N; i++) {
    data_[i] += other.data_[i];
  }
  return *this;
}

template <typename Type, int N>
Vector<Type, N> &
Vector<Type, N>::operator-=(const Vector<Type, N> &other) noexcept {
  return *this += other * static_cast<Type>(-1);
}

template <typename Type, int N>
Vector<Type, N> Vector<Type, N>::operator*(Type num) const noexcept {
  Vector<Type, N> v;
  for (int i = 0; i < N; i++) {
    v[i] = data_[i] * num;
  }
  return v;
}

template <typename Type, int N>
Vector<Type, N> Vector<Type, N>::operator/(Type num) const noexcept {
  return *this * (static_cast<Type>(1) / num);
}

template <typename Type, int N>
Vector<Type, N>
Vector<Type, N>::operator+(const Vector<Type, N> &other) const noexcept {
  Vector<Type, N> v;
  for (int i = 0; i < N; i++) {
    v[i] = other.data_[i] + data_[i];
  }
  return v;
}

template <typename Type, int N>
Vector<Type, N>
Vector<Type, N>::operator-(const Vector<Type, N> &other) const noexcept {
  return *this + other * static_cast<Type>(-1);
}

constexpr int Dynamic = -1;
template <typename Type> class Vector<Type, Dynamic> {
  std::vector<Type> data_;
  std::size_t size_ = 0;

public:
  constexpr Vector() = default;
  constexpr explicit Vector(const std::size_t &size);
  constexpr Vector(const Vector<Type, Dynamic> &other);
  constexpr Vector(std::initializer_list<Type> s);

  const Type &operator[](unsigned int i) const noexcept { return data_[i]; }
  Type &operator[](unsigned int i) noexcept { return data_[i]; }

  Type maxNorm() const noexcept;
  Type norm() const noexcept;
  Type dot(const Vector<Type, Dynamic> &other) const noexcept;

  [[nodiscard]] constexpr std::size_t getSize() const noexcept { return size_; }
  bool operator==(const Vector<Type, Dynamic> &other) noexcept;
  Vector<Type, Dynamic> &operator*=(Type num) noexcept;
  Vector<Type, Dynamic> &operator/=(Type num) noexcept;
  Vector<Type, Dynamic> &
  operator+=(const Vector<Type, Dynamic> &other) noexcept;
  Vector<Type, Dynamic> &
  operator-=(const Vector<Type, Dynamic> &other) noexcept;

  Vector<Type, Dynamic> operator*(Type num) const noexcept;
  Vector<Type, Dynamic> operator/(Type num) const noexcept;
  Vector<Type, Dynamic>
  operator+(const Vector<Type, Dynamic> &other) const noexcept;
  Vector<Type, Dynamic>
  operator-(const Vector<Type, Dynamic> &other) const noexcept;

  friend Vector<Type, Dynamic> operator*(Type num,
                                         const Vector<Type, Dynamic> &vec) {
    return vec * num;
  }
};

template <typename Type>
constexpr Vector<Type, Dynamic>::Vector(const std::size_t &size)
    : data_(std::vector<Type>(size)), size_(size) {}
template <typename Type>
constexpr Vector<Type, Dynamic>::Vector(const Vector<Type, Dynamic> &other)
    : data_(other.data_), size_(other.size_) {}
template <typename Type>
constexpr Vector<Type, Dynamic>::Vector(std::initializer_list<Type> s)
    : data_(std::vector<Type>(s.size())), size_(s.size()) {
  for (int i = 0; i < size_; i++) {
    data_[i] = *(s.begin() + i);
  }
}

template <typename Type>
bool Vector<Type, Dynamic>::operator==(const Vector<Type, Dynamic> &other) noexcept{
  if (size_ != other.size_) {
    return false;
  }
  for (int i = 0; i < size_; i++) {
    if (data_[i] != other.data_[i]) {
      return false;
    }
  }
  return true;
}

template <typename Type> Type Vector<Type, Dynamic>::maxNorm() const noexcept{
  Type p = std::abs(data_[0]);
  for (const auto& elem: data_) {
    if(p<std::abs(elem)){
      p = std::abs(elem);
    }
  }
  return p;
}

template <typename Type> Type Vector<Type, Dynamic>::norm() const noexcept{
  Type p = 0;
  for (const auto& elem: data_) {
    p += elem*elem;
  }
  return std::sqrt(p);
}

template <typename Type>
Type Vector<Type, Dynamic>::dot(const Vector<Type, Dynamic> &other) const noexcept{
  Type dot_pr = 0;
  for (int i = 0; i < size_; ++i) {
    dot_pr += data_[i] * other.data_[i];
  }
  return dot_pr;
}

template <typename Type>
Vector<Type, Dynamic> &Vector<Type, Dynamic>::operator*=(Type num) noexcept{
  for (auto &elem: data_) {
    elem *= num;
  }
  return *this;
}

template <typename Type>
Vector<Type, Dynamic> &Vector<Type, Dynamic>::operator/=(Type num) noexcept{
  return ((*this) *= (static_cast<Type>(1) / num));
}

template <typename Type>
Vector<Type, Dynamic> &
Vector<Type, Dynamic>::operator+=(const Vector<Type, Dynamic> &other) noexcept{
  for (int i = 0; i < size_; i++) {
    data_[i] += other.data_[i];
  }
  return *this;
}

template <typename Type>
Vector<Type, Dynamic> &
Vector<Type, Dynamic>::operator-=(const Vector<Type, Dynamic> &other) noexcept{
  return *this += other * static_cast<Type>(-1);
}

template <typename Type>
Vector<Type, Dynamic> Vector<Type, Dynamic>::operator*(Type num) const noexcept{
  Vector<Type, Dynamic> v{*this};
  for (int i = 0; i < size_; i++) {
    v[i] = data_[i] * num;
  }
  return v;
}
template <typename Type>
Vector<Type, Dynamic> Vector<Type, Dynamic>::operator/(Type num) const noexcept{
  return *this * (static_cast<Type>(1) / num);
}

template <typename Type>
Vector<Type, Dynamic>
Vector<Type, Dynamic>::operator+(const Vector<Type, Dynamic> &other) const noexcept{
  Vector<Type, Dynamic> v{*this};
  for (int i = 0; i < size_; i++) {
    v[i] = other.data_[i] + data_[i];
  }
  return v;
}

template <typename Type>
Vector<Type, Dynamic>
Vector<Type, Dynamic>::operator-(const Vector<Type, Dynamic> &other) const noexcept{
  return *this + other * static_cast<Type>(-1);
}

#endif // VECTOR_H
