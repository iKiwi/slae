//
// Created by ivan on 04.03.23.
//

#ifndef SLAE_ITER_HPP
#define SLAE_ITER_HPP

#include "matrices/CSR.hpp"
#include "types/Vector.hpp"

template <size_t i> constexpr std::array<std::size_t, i> ChebyshevShake() {
  static_assert(i % 2 == 0);
  std::array<std::size_t, i / 2> prev_row = ChebyshevShake<i / 2>();
  std::array<std::size_t, i> row{};
  for (int j = 0; j < prev_row.size(); j++) {
    row[j * 2] = prev_row[j];
    row[j * 2 + 1] = i - prev_row[j] - 1;
  }
  return row;
}

template <> constexpr std::array<std::size_t, 2> ChebyshevShake<2>() {
  std::array<size_t, 2> row = {0, 1};
  return row;
}

template <typename T, size_t N> std::array<T, N> ChebyshevRoots() {
  // std::cos is not constexpr yet
  // implementation might be constexpr since c++23
  std::array<T, N> rootsInverse;
  rootsInverse[0] = std::cos(M_PI / (static_cast<T>(2) * N));
  rootsInverse[N - 1] = -rootsInverse[0];
  T yk = std::sin(M_PI / (static_cast<T>(2) * N));
  T c =
      static_cast<T>(2) * rootsInverse[0] * rootsInverse[0] - static_cast<T>(1);
  T s = static_cast<T>(2) * rootsInverse[0] * yk;

  for (int i = 1; i < N / 2; ++i) {
    rootsInverse[i] = rootsInverse[i - 1] * c - yk * s;
    yk = rootsInverse[i - 1] * s + yk * c;
    rootsInverse[N - i - 1] = -rootsInverse[i];
  }
  return rootsInverse;
}

template <typename T>
T calcResidual(const Vector<T, -1> &x, const CSR<T> &A,
               const Vector<T, -1> &b) {
  return (A * x - b).maxNorm();
}

template <typename T>
void IterGaussSeidel(const std::vector<T> &rows,
                     const std::vector<v_c<T>> &val_col, const size_t m,
                     const Vector<T, -1> &b, Vector<T, -1> &x_i) {
  for (int j = 0; j < m; ++j) {
    x_i[j] = b[j];
    T Ajj;
    for (int k = rows[j]; k < rows[j + 1]; ++k) {
      j == val_col[k].c ? Ajj = val_col[k].v
                        : x_i[j] -= val_col[k].v * x_i[val_col[k].c];
    }
    x_i[j] /= Ajj;
  }
}

template <typename T>
Vector<T, -1> GaussSeidel(const Vector<T, -1> &x, const CSR<T> &A,
                          const Vector<T, -1> &b, T eps) {
  std::size_t m = x.getSize();
  Vector<T, -1> x_i = x;
  // A params
  const std::vector<T> &rows = A.getRows();
  const std::vector<v_c<T>> &val_col = A.getValCol();

  while (calcResidual(x_i, A, b) > eps) {
    IterGaussSeidel(rows, val_col, m, b, x_i);
  }
  return x_i;
}

template <typename T>
Vector<T, -1> Jacobi(const Vector<T, -1> &x, const CSR<T> &A,
                     const Vector<T, -1> &b, T eps) {
  std::size_t m = x.getSize();
  Vector<T, -1> x_prev = x;
  // A params
  const std::vector<T> &rows = A.getRows();
  const std::vector<v_c<T>> &val_col = A.getValCol();

  while (calcResidual(x_prev, A, b) > eps) {
    Vector<T, -1> x_next(m);
    std::vector<T> Aj(m);
    //(L+U)x_i
    for (int j = 0; j < m; ++j) {
      for (int k = rows[j]; k < rows[j + 1]; ++k) {
        j == val_col[k].c ? Aj[j] = val_col[k].v
                          : x_next[j] += val_col[k].v * x_prev[val_col[k].c];
      }
    }
    // b - (L+U)x_i
    x_next = b - x_next;
    // D-1(b - (L+U)x_i)
    for (int j = 0; j < m; ++j) {
      x_next[j] /= Aj[j];
    }
    x_prev = x_next;
  }
  return x_prev;
}

template <typename T>
Vector<T, -1> SimpleIter(const Vector<T, -1> &x, const CSR<T> &A,
                         const Vector<T, -1> &b, T tau, T eps) {
  Vector<T, -1> x_i = x;
  while (calcResidual(x_i, A, b) > eps) {
    x_i -= tau * (A * x_i - b);
  }
  return x_i;
}

template <typename T>
Vector<T, -1> ChebyshevSIM(const Vector<T, -1> &x, const CSR<T> &A,
                           const Vector<T, -1> &b, T eps, T l_min, T l_max) {
  constexpr size_t N = 8;
  constexpr std::array<size_t, N> row = ChebyshevShake<N>();
  // constexpr soon
  const std::array<T, N> rootsInverse = ChebyshevRoots<T, N>();

  T q = (l_max + l_min) / static_cast<T>(2);
  T p = (l_max - l_min) / static_cast<T>(2);

  Vector<T, -1> x_i = x;
  while (calcResidual(x_i, A, b) > eps) {
    for (int i = 0; i < N; ++i) {
      x_i -= static_cast<T>(1) / (q + p * rootsInverse[row[i]]) * (A * x_i - b);
    }
  }
  return x_i;
}

template <typename T>
void IterSOR_1(const std::vector<T> &rows, const std::vector<v_c<T>> &val_col,
               const size_t m, const Vector<T, -1> &b, const T w,
               Vector<T, -1> &x_i) {

  for (int j = 0; j < m; ++j) {
    T x_j = x_i[j];
    x_i[j] = b[j];
    T Ajj;
    for (int k = rows[j]; k < rows[j + 1]; ++k) {
      j == val_col[k].c ? Ajj = val_col[k].v
                        : x_i[j] -= val_col[k].v * x_i[val_col[k].c];
    }
    x_i[j] *= w / Ajj;
    x_i[j] += (static_cast<T>(1) - w) * x_j;
  }
}

template <typename T>
void IterSOR_2(const std::vector<T> &rows, const std::vector<v_c<T>> &val_col,
               const size_t m, const Vector<T, -1> &b, const T w,
               Vector<T, -1> &x_i) {

  for (int j = static_cast<int>(m) - 1; j >= 0; --j) {
    T x_j = x_i[j];
    x_i[j] = b[j];
    T Ajj;
    for (int k = rows[j]; k < rows[j + 1]; ++k) {
      j == val_col[k].c ? Ajj = val_col[k].v
                        : x_i[j] -= val_col[k].v * x_i[val_col[k].c];
    }
    x_i[j] *= w / Ajj;
    x_i[j] += (static_cast<T>(1) - w) * x_j;
  }
}

template <typename T>
Vector<T, -1> SOR(const Vector<T, -1> &x, const CSR<T> &A,
                  const Vector<T, -1> &b, T w, T eps) {
  std::size_t m = x.getSize();
  Vector<T, -1> x_i = x;
  // A params
  const std::vector<T> &rows = A.getRows();
  const std::vector<v_c<T>> &val_col = A.getValCol();

  while (calcResidual(x_i, A, b) > eps) {
    IterSOR_1(rows, val_col, m, b, w, x_i);
  }
  return x_i;
}

template <typename T>
Vector<T, -1> SSOR(const Vector<T, -1> &x, const CSR<T> &A,
                   const Vector<T, -1> &b, T w, T eps) {
  std::size_t m = x.getSize();
  Vector<T, -1> x_i = x;
  // A params
  const std::vector<T> &rows = A.getRows();
  const std::vector<v_c<T>> &val_col = A.getValCol();

  while (calcResidual(x_i, A, b) > eps) {
    IterSOR_1(rows, val_col, m, b, w, x_i);
    IterSOR_2(rows, val_col, m, b, w, x_i);
  }
  return x_i;
}
#endif // SLAE_TRIDIAGONALSOLVER_HPP