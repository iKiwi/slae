//
// Created by ivan on 07.02.23.
//

#ifndef SLAE_TRIDIAGONALSOLVER_HPP
#define SLAE_TRIDIAGONALSOLVER_HPP

#include "matrices/Tridiagonal.hpp"
#include "types/Vector.hpp"

template <typename T> struct pq {
  T p;
  T q;
};

template <typename T, int N>
Vector<T, N> TridiagonalSolver(const ThreeDiagonal<T, N> &A,
                               const Vector<T, N> &b) {
  Vector<T, N> p, q;

  p[0] = -A[0].right / A[0].middle;
  q[0] = b[0] / A[0].middle;
  // calc p_i,q_i
  for (int i = 1; i < N - 1; i++) {
    T div = (A[i].left * p[i - 1] + A[i].middle);
    p[i] = -A[i].right / div;
    q[i] = (b[i] - A[i].left * q[i - 1]) / div;
  }
  // x_n
  p[N - 1] = (b[N - 1] - A[N - 1].left * q[N - 2]) /
             (A[N - 1].left * p[N - 2] + A[N - 1].middle);
  // calc x_i
  for (int i = N - 2; i >= 0; --i) {
    p[i] = p[i] * p[i + 1] + q[i];
  }
  return p;
}

template <typename T>
Vector<T, -1> TridiagonalSolver(const ThreeDiagonal<T, -1> &A,
                                const Vector<T, -1> &b) {
  int N = b.getSize();
  Vector<T, -1> p(N), q(N);

  p[0] = -A[0].right / A[0].middle;
  q[0] = b[0] / A[0].middle;
  // calc p_i,q_i
  for (int i = 1; i < N - 1; i++) {
    T div = (A[i].left * p[i - 1] + A[i].middle);
    p[i] = -A[i].right / div;
    q[i] = (b[i] - A[i].left * q[i - 1]) / div;
  }
  // x_n
  p[N - 1] = (b[N - 1] - A[N - 1].left * q[N - 2]) /
             (A[N - 1].left * p[N - 2] + A[N - 1].middle);
  // calc x_i
  for (int i = N - 2; i >= 0; --i) {
    p[i] = p[i] * p[i + 1] + q[i];
  }
  return p;
}

#endif // SLAE_TRIDIAGONALSOLVER_HPP