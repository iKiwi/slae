#ifndef SLAE_HOUSEHOLDER_HPP
#define SLAE_HOUSEHOLDER_HPP

#include "matrices/DenseMatrix.hpp"
#include "types/Vector.hpp"
#include <vector>

template <typename T, int M, int N> struct QR {
  DenseMatrix<T, M, M> Q;
  DenseMatrix<T, M, N> R;
};

template <typename T> struct QR<T, Dynamic, Dynamic> {
  DenseMatrix<T, Dynamic, Dynamic> Q;
  DenseMatrix<T, Dynamic, Dynamic> R;
};

template <typename T, int M, int N>
QR<T, M, N> HouseHolder(DenseMatrix<T, M, N> R) {
  static_assert(M >= N, "Row size should be not less than column size");
  DenseMatrix<T, M, M> Q;

  for (int i = 0; i < N; ++i) {
    std::vector<double> nVector(M - i);
    T aii = R(i, i);
    T ww = aii * aii;
    nVector[0] = aii;
    for (int j = i + 1; j < M; ++j) {
      T a = R(j, i);
      ww += a * a;
      nVector[j - i] = a;
      R(j, i) = 0;
    }
    T w = std::sqrt(ww);
    nVector[0] += R(i, i) > 0 ? w : -1 * w;
    T nn = 2 * ww + 2 * w * std::abs(aii);
    R(i, i) -= nVector[0];
    for (int k = N - 1; k > i; --k) {
      T nw = 0;
      for (int j = i; j < M; ++j) {
        nw += nVector[j - i] * R(j, k);
      }
      for (int j = i; j < M; ++j) {
        R(j, k) -= 2 * nw / nn * nVector[j - i];
      }
    }

    if (i == 0) {
      for (int k = 0; k < M; ++k) {
        for (int j = k; j < M; ++j) {
          k == j ? Q(k, k) = 1 - 2 * nVector[k] * nVector[k] / nn
                 : Q(k, j) = (Q(j, k) = -2 * nVector[k] * nVector[j] / nn);
        }
      }
    } else {
      for (int k = 0; k < M; ++k) {
        T nw = 0;
        for (int j = i; j < M; ++j) {
          nw += nVector[j - i] * Q(k, j);
        }
        for (int j = i; j < M; ++j) {
          Q(k, j) -= 2 * nw / nn * nVector[j - i];
        }
      }
    }
  }

  return {Q, R};
}

#endif // SLAE_HOUSEHOLDER_HPP