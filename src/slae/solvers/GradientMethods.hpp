//
// Created by ivan on 07.04.23.
//

#ifndef SLAE_GRADIENTMETHODS_HPP
#define SLAE_GRADIENTMETHODS_HPP

#include "IterationSolvers.hpp"
#include "matrices/CSR.hpp"
#include "types/Vector.hpp"

template <typename T, typename MatrixType>
Vector<T, -1> HeavyBall(const Vector<T, -1> &x0, const MatrixType &A,
                        const Vector<T, -1> &b, T eps) {
  Vector<T, -1> x = x0;
  Vector<T, -1> y = x0;
  Vector<T, -1> x_prev = x0;
  Vector<T, -1> r = A * x - b;
  Vector<T, -1> delta;
  T alpha = (r.dot(r)) / (r.dot(A * r));
  T beta;
  while (r.maxNorm() > eps) {
    x = y - alpha * r;
    delta = x - x_prev;
    r = A * x - b;
    Vector<T, -1> Ar = A * r;
    beta = (alpha * (Ar)-r).dot(r) / (delta.dot(Ar));
    y = x + beta * delta;
    alpha = (beta * delta.dot(Ar) + r.dot(r)) / (r.dot(Ar));
    x_prev = x;
  }
  return x;
}

template <typename T, typename MatrixType>
Vector<T, -1> GradientDescent(const Vector<T, -1> &x0, const MatrixType &A,
                              const Vector<T, -1> &b, T eps) {
  Vector<T, -1> x = x0;
  Vector<T, -1> r = A * x - b;
  T alpha;
  while (r.maxNorm() > eps) {
    alpha = r.dot(r) / r.dot(A * r);
    x -= alpha * r;
    r = A * x - b;
  }
  return x;
}

template <typename T, typename MatrixType>
Vector<T, -1> CG(const Vector<T, -1> &x0, const MatrixType &A, const Vector<T, -1> &b,
                 T eps) {
  Vector<T, -1> x = x0;
  Vector<T, -1> r = A * x - b;
  Vector<T, -1> d = r;
  int m = b.getSize();
  T rsq = r.dot(r);

  for (int i = 0; i < m; ++i) {
    Vector<T, -1> Ad = A * d;
    T alpha = rsq / d.dot(Ad);
    x -= alpha * d;
    r -= alpha * Ad;
    if (r.maxNorm() < eps) {
      break;
    } else {
      T rsq_ = r.dot(r);
      d = r + (rsq_ / rsq) * d;
      rsq = rsq_;
    }
  }
  return x;
}

#endif // SLAE_GRADIENTMETHODS_HPP
