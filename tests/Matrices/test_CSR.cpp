#include <gtest/gtest.h>
#include "matrices/CSR.hpp"
#include <vector>


TEST(csr, initVecDynamic){
  std::vector<v_c<double>> V_c = {{10, 0}, {20,1}, {30, 1}, {40, 3}, {50, 2}, {60, 3}, {70, 4}, {80, 5}};
  std::vector<double> r = {0,  2,  4,  7,  8};

  CSR<double> a(6, V_c, r);
  ASSERT_TRUE(a.getRowNum() == 4);
  ASSERT_TRUE(a.getColumnNum() == 6);
  ASSERT_TRUE(a(0, 1) == 20);
  ASSERT_TRUE(a(2,4) == 70);
}


TEST(csr, multiplicationDynamic){
  std::vector<v_c<double>> V_c = {{5, 0}, {8,1}, {3, 2}, {6, 1}};
  std::vector<double> r = {0, 1, 2, 3, 4};

  CSR<double> A(4, V_c, r);
  Vector<double, -1>  vec = {1, 1, 1, 2};
  Vector<double, -1> res = A*vec;

  ASSERT_TRUE(res[0] == 5);
  ASSERT_TRUE(res[1] == 8);
  ASSERT_TRUE(res[2] == 3);
  ASSERT_TRUE(res[3] == 6);
}
