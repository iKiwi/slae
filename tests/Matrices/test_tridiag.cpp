#include "matrices/Tridiagonal.hpp"
#include "solvers/TridiagonalSolver.hpp"
#include "types/Vector.hpp"
#include <gtest/gtest.h>
#include <limits>

#define _USE_MATH_DEFINES

TEST(Tridiagonal, init) {
  Vector<int, 1> upper_2a{1}, lower_2a{4};
  Vector<int, 2> upper_3a{1, 2}, middle_2a{2, 3}, lower_3a{6, 7};
  Vector<int, 3> upper_4a{1, 2, 3}, middle_3a{3, 4, 5}, lower_4a{8, 9, 10};
  Vector<int, 4> middle_4a{4, 5, 6, 7};

  Vector<int, -1> upper_2v{1}, upper_3v{1, 2}, upper_4v{1, 2, 3};
  Vector<int, -1> middle_2v{2, 3}, middle_3v{3, 4, 5}, middle_4v{4, 5, 6, 7};
  Vector<int, -1> lower_2v{4}, lower_3v{6, 7}, lower_4v{8, 9, 10};

  ThreeDiagonal<int, 2> Ta2(lower_2a, middle_2a, upper_2a);
  ThreeDiagonal<int, 3> Ta3(lower_3a, middle_3a, upper_3a);
  ThreeDiagonal<int, 4> Ta4(lower_4a, middle_4a, upper_4a);

  ThreeDiagonal<int, -1> Tv2(lower_2v, middle_2v, upper_2v);
  ThreeDiagonal<int, -1> Tv3(lower_3v, middle_3v, upper_3v);
  ThreeDiagonal<int, -1> Tv4(lower_4v, middle_4v, upper_4v);

  ASSERT_TRUE((Ta2[0].middle == 2) && (Ta2[1].left == 4));
  ASSERT_TRUE((Ta3[0].middle == 3) && (Ta3[1].right == 2));
  ASSERT_TRUE((Ta4[2].left == 9) && (Ta4[1].middle == 5));

  ASSERT_TRUE((Tv2[0].middle == 2) && (Tv2[1].left == 4));
  ASSERT_TRUE((Tv3[0].middle == 3) && (Tv3[1].right == 2));
  ASSERT_TRUE((Tv4[2].left == 9) && (Tv4[1].middle == 5));

  ASSERT_TRUE((Ta4(2,1) == 9));
  ASSERT_TRUE((Ta4(2,0) == 0));
  ASSERT_TRUE((Ta4(1,1) == 5));

  ASSERT_TRUE((Tv4(2,1) == 9));
  ASSERT_TRUE((Tv4(2,0) == 0));
  ASSERT_TRUE((Tv4(1,1) == 5));
}


TEST(Tridiagonal, solve1s) {
  Vector<double, 3> middle{5, 7, 9};
  Vector<double, 2> upper{-2, -4}, lower{-2, 1};
  Vector<double, 3> b{1, 0, 29};
  auto x = TridiagonalSolver(ThreeDiagonal<double, 3>(lower, middle, upper), b);
  ASSERT_NEAR(x[0], 1., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[1], 2., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[2], 3., 100 * std::numeric_limits<double>::epsilon());
}

TEST(Tridiagonal, solve1d) {
  Vector<double, -1> middle{5, 7, 9};
  Vector<double, -1> upper{-2, -4}, lower{-2, 1};
  Vector<double, -1> b{1, 0, 29};
  auto x =
      TridiagonalSolver(ThreeDiagonal<double, -1>(lower, middle, upper), b);
  ASSERT_NEAR(x[0], 1., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[1], 2., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[2], 3., 100 * std::numeric_limits<double>::epsilon());
}

TEST(Tridiagonal, solve2s) {
  Vector<double, 4> middle{1, 9, M_PI, 10};
  Vector<double, 3> upper{1, 2, 7}, lower{0, 4, 1};
  Vector<double, 4> b{3, 24, 36 + 3 * M_PI, 43};
  auto x = TridiagonalSolver(ThreeDiagonal<double, 4>(lower, middle, upper), b);
  ASSERT_NEAR(x[0], 1., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[1], 2., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[2], 3., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[3], 4., 100 * std::numeric_limits<double>::epsilon());
}

TEST(Tridiagonal, solve2d) {
  Vector<double, -1> middle{1, 9, M_PI, 10};
  Vector<double, -1> upper{1, 2, 7}, lower{0, 4, 1};
  Vector<double, -1> b{3, 24, 36 + 3 * M_PI, 43};
  auto x =
      TridiagonalSolver(ThreeDiagonal<double, -1>(lower, middle, upper), b);

  ASSERT_NEAR(x[0], 1., 100 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[1], 2., 200 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[2], 3., 300 * std::numeric_limits<double>::epsilon());
  ASSERT_NEAR(x[3], 4., 400 * std::numeric_limits<double>::epsilon());
}
