#include "matrices/DenseMatrix.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(Dense, StaticInit){
  DenseMatrix<int, 3, 3> A;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<3; ++j){
      A(i, j) = i*j;
    }
  }
  ASSERT_TRUE(A(1,1) == 1);
  ASSERT_TRUE(A(1,2) == 2);
  ASSERT_TRUE(A(1,0) == 0);
  ASSERT_TRUE(A(2,2) == 4);

  DenseMatrix<double, 3, 5> B;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }
  ASSERT_DOUBLE_EQ(B(1,1), 0.1);
  ASSERT_DOUBLE_EQ(B(1,2), 0.2);
  ASSERT_DOUBLE_EQ(B(1,0), 0.);
  ASSERT_DOUBLE_EQ(B(2,2), 0.4);
  ASSERT_DOUBLE_EQ(B(2,3), 0.6);
}

TEST(Dense, StaticOperators){
  DenseMatrix<double, 3, 5> B;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }
  B+=(B*3.);
  B/=2.;
  (B-=(B/2.));
  B*=3.;
  B = B+B;
  B = B/6.;


  ASSERT_DOUBLE_EQ(B(1,1), 0.1);
  ASSERT_DOUBLE_EQ(B(1,2), 0.2);
  ASSERT_DOUBLE_EQ(B(1,0), 0.);
  ASSERT_DOUBLE_EQ(B(2,2), 0.4);
  ASSERT_DOUBLE_EQ(B(2,3), 0.6);
}


TEST(Dense, DynamicInit){
  DenseMatrix<int, -1, -1> A(3, 3);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<3; ++j){
      A(i, j) = i*j;
    }
  }
  ASSERT_TRUE(A(1,1) == 1);
  ASSERT_TRUE(A(1,2) == 2);
  ASSERT_TRUE(A(1,0) == 0);
  ASSERT_TRUE(A(2,2) == 4);

  DenseMatrix<double, -1, -1> B(3, 5);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }
  ASSERT_DOUBLE_EQ(B(1,1), 0.1);
  ASSERT_DOUBLE_EQ(B(1,2), 0.2);
  ASSERT_DOUBLE_EQ(B(1,0), 0.);
  ASSERT_DOUBLE_EQ(B(2,2), 0.4);
  ASSERT_DOUBLE_EQ(B(2,3), 0.6);
}

TEST(Dense, DynamicOperators){
  DenseMatrix<double, -1, -1> B(3, 5);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }
  B+=(B*3.);
  B/=2.;
  (B-=(B/2.));
  B*=3.;
  B = B+B;
  B = B/6.;


  ASSERT_DOUBLE_EQ(B(1,1), 0.1);
  ASSERT_DOUBLE_EQ(B(1,2), 0.2);
  ASSERT_DOUBLE_EQ(B(1,0), 0.);
  ASSERT_DOUBLE_EQ(B(2,2), 0.4);
  ASSERT_DOUBLE_EQ(B(2,3), 0.6);
}



TEST(Dense, StaticMul){
  DenseMatrix<double, 3, 5> A;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      A(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }

  DenseMatrix<double, 5, 2> B;
  for(int i = 0; i<5; ++i){
    for(int j = 0; j<2; ++j){
      B(i, j) = static_cast<double>((i+1)*(j+1));
    }
  }

  Vector<double, 5> D = {1., 2., 3., 4., 5.};

  Vector<double, 3> E = A*D;
  DenseMatrix<double, 3, 2> C = A*B;

  ASSERT_DOUBLE_EQ(C(1,1), 8.);
  ASSERT_DOUBLE_EQ(C(0,0), 0.);
  ASSERT_DOUBLE_EQ(C(1,0), 4.);
  ASSERT_DOUBLE_EQ(C(2,1), 16);

  ASSERT_DOUBLE_EQ(E[0], 0);
  ASSERT_DOUBLE_EQ(E[1], 4);
  ASSERT_DOUBLE_EQ(E[2], 8);
}


TEST(Dense, DynamicMul){
  DenseMatrix<double, -1, -1> A(3, 5);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      A(i, j) = 0.1 * static_cast<double>(i*j);
    }
  }

  DenseMatrix<double, -1, -1> B(5, 2);
  for(int i = 0; i<5; ++i){
    for(int j = 0; j<2; ++j){
      B(i, j) = static_cast<double>((i+1)*(j+1));
    }
  }

  Vector<double, -1> D = {1., 2., 3., 4., 5.};

  Vector<double, -1> E = A*D;
  DenseMatrix<double, -1, -1> C = A*B;

  ASSERT_DOUBLE_EQ(C(1,1), 8.);
  ASSERT_DOUBLE_EQ(C(0,0), 0.);
  ASSERT_DOUBLE_EQ(C(1,0), 4.);
  ASSERT_DOUBLE_EQ(C(2,1), 16);

  ASSERT_DOUBLE_EQ(E[0], 0);
  ASSERT_DOUBLE_EQ(E[1], 4);
  ASSERT_DOUBLE_EQ(E[2], 8);
}

TEST(Dense, StaticTranspose){
  DenseMatrix<int, 3, 3> A;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<3; ++j){
      A(i, j) = i-j;
    }
  }
  auto AT = A.transpose();
  ASSERT_TRUE(AT(1,1) == 0);
  ASSERT_TRUE(AT(2,1) == -1);
  ASSERT_TRUE(AT(0,1) == 1);
  ASSERT_TRUE(AT(2,2) == 0);



  DenseMatrix<double, 3, 5> B;
  for(int i = 0; i<3; ++i){
    for(int j = 0; j < 5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i - j);
    }
  }
  auto BT = 2.*B.transpose();
  ASSERT_DOUBLE_EQ(BT(1,1), 0.);
  ASSERT_DOUBLE_EQ(BT(2,1), -0.2);
  ASSERT_DOUBLE_EQ(BT(0,1), 0.2);
  ASSERT_DOUBLE_EQ(BT(2,2), 0.);
  ASSERT_DOUBLE_EQ(BT(3,2), -0.2);
}

TEST(Dense, DynamicTranspose){
  DenseMatrix<int, -1, -1> A(3, 3);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<3; ++j){
      A(i, j) = i-j;
    }
  }
  auto AT = A.transpose();
  ASSERT_TRUE(AT(1,1) == 0);
  ASSERT_TRUE(AT(2,1) == -1);
  ASSERT_TRUE(AT(0,1) == 1);
  ASSERT_TRUE(AT(2,2) == 0);

  DenseMatrix<double, -1, -1> B(3, 5);
  for(int i = 0; i<3; ++i){
    for(int j = 0; j<5; ++j){
      B(i, j) = 0.1 * static_cast<double>(i-j);
    }
  }

  auto BT = 2.*B.transpose();
  ASSERT_DOUBLE_EQ(BT(1,1), 0.);
  ASSERT_DOUBLE_EQ(BT(2,1), -0.2);
  ASSERT_DOUBLE_EQ(BT(0,1), 0.2);
  ASSERT_DOUBLE_EQ(BT(2,2), 0.);
  ASSERT_DOUBLE_EQ(BT(3,2), -0.2);
}