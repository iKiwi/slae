//
// Created by ivan on 08.04.23.
//

#include "iomanip"
#include "solvers/GradientMethods.hpp"
#include "solvers/IterationSolvers.hpp"
#include "types/Vector.hpp"
#include <gtest/gtest.h>

TEST(TASK1, 1) {
  const double a_ = 2;
  const double b_ = 6;
  const double c_ = 4;

  constexpr size_t L = 17;
  constexpr size_t N = L * L;
  std::set<matrix_element<double>> init;

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      if (i == j) {
        init.insert({static_cast<size_t>(i), static_cast<size_t>(j), 2 * b_});
      }
      if (std::abs(i - j) == 1) {
        init.insert({static_cast<size_t>(i), static_cast<size_t>(j), a_});
      }
      if (std::abs(i - j) == L) {
        init.insert({static_cast<size_t>(i), static_cast<size_t>(j), a_});
      }
    }
  }

  Vector<double, -1> b(N);
  Vector<double, -1> x0(N);

  for (int i = 0; i < N; ++i) {
    b[i] = c_;
    x0[i] = 0;
  }

  CSR<double> A(N, N, init);
  double L_min = 2. * (b_ - 2. * a_ * std::cos(M_PI / (L + 1)));
  double L_max = 2. * (b_ + 2. * a_ * std::cos(M_PI / (L + 1)));

  double eps = 1e-13;
  Vector<double, -1> resCG = CG(x0, A, b, eps);
  Vector<double, -1> resSIM = SimpleIter(x0, A, b, 1 / L_max, eps);
  Vector<double, -1> resSIM_opt =
      SimpleIter(x0, A, b, 2 / (L_max + L_min), eps);
  Vector<double, -1> resSSOR = SSOR(x0, A, b, 1., eps);
  Vector<double, -1> resCheb = ChebyshevSIM(x0, A, b, eps, L_min, L_max);

  ASSERT_NEAR((A * resCG)[0], b[0], 300 * eps);
  ASSERT_NEAR((A * resCG)[21], b[21], 300 * eps);
  ASSERT_NEAR((A * resCG)[199], b[199], 300 * eps);

  ASSERT_NEAR((A * resSIM)[0], b[0], 300 * eps);
  ASSERT_NEAR((A * resSIM)[21], b[21], 300 * eps);
  ASSERT_NEAR((A * resSIM)[199], b[199], 300 * eps);

  ASSERT_NEAR((A * resSIM_opt)[0], b[0], 300 * eps);
  ASSERT_NEAR((A * resSIM_opt)[21], b[21], 300 * eps);
  ASSERT_NEAR((A * resSIM_opt)[199], b[199], 300 * eps);

  ASSERT_NEAR((A * resSSOR)[0], b[0], 300 * eps);
  ASSERT_NEAR((A * resSSOR)[21], b[21], 300 * eps);
  ASSERT_NEAR((A * resSSOR)[199], b[199], 300 * eps);

  ASSERT_NEAR((A * resCheb)[0], b[0], 300 * eps);
  ASSERT_NEAR((A * resCheb)[21], b[21], 300 * eps);
  ASSERT_NEAR((A * resCheb)[199], b[199], 300 * eps);
}

TEST(TASK2, 1) {
  std::vector<v_c<double>> V_c = {
      {2, 0},
      {2, 1},
      {2, 2},
      {3, 3},
  };
  std::vector<double> r = {0, 1, 2, 3, 4};
  CSR<double> A(3, V_c, r);
  Vector<double, -1> b = {1, 1, 1, 1};
  Vector<double, -1> x0 = {0, 0, 0, 0};
  double eps = 1e-13;
  double L_min = 2;
  double L_max = 3;

  Vector<double, -1> resSIM = SimpleIter(x0, A, b, (2 * 0.9) / L_max, eps);
  Vector<double, -1> resCG = CG(x0, A, b, eps);
  Vector<double, -1> resSIM_opt =
      SimpleIter(x0, A, b, 2 / (L_max + L_min), eps);
  Vector<double, -1> resHVB = HeavyBall(x0, A, b, eps);
  Vector<double, -1> resGD = GradientDescent(x0, A, b, eps);

  ASSERT_NEAR((A * resCG)[0], b[0], 10 * eps);
  ASSERT_NEAR((A * resCG)[1], b[1], 10 * eps);
  ASSERT_NEAR((A * resCG)[2], b[2], 10 * eps);
  ASSERT_NEAR((A * resCG)[3], b[3], 10 * eps);

  ASSERT_NEAR((A * resSIM)[0], b[0], 10 * eps);
  ASSERT_NEAR((A * resSIM)[1], b[1], 10 * eps);
  ASSERT_NEAR((A * resSIM)[2], b[2], 10 * eps);
  ASSERT_NEAR((A * resSIM)[3], b[3], 10 * eps);

  ASSERT_NEAR((A * resHVB)[0], b[0], 10 * eps);
  ASSERT_NEAR((A * resHVB)[1], b[1], 10 * eps);
  ASSERT_NEAR((A * resHVB)[2], b[2], 10 * eps);
  ASSERT_NEAR((A * resHVB)[3], b[3], 10 * eps);

  ASSERT_NEAR((A * resGD)[0], b[0], 10 * eps);
  ASSERT_NEAR((A * resGD)[1], b[1], 10 * eps);
  ASSERT_NEAR((A * resGD)[2], b[2], 10 * eps);
  ASSERT_NEAR((A * resGD)[3], b[3], 10 * eps);

  ASSERT_NEAR((A * resSIM_opt)[0], b[0], 10 * eps);
  ASSERT_NEAR((A * resSIM_opt)[1], b[1], 10 * eps);
  ASSERT_NEAR((A * resSIM_opt)[2], b[2], 10 * eps);
  ASSERT_NEAR((A * resSIM_opt)[3], b[3], 10 * eps);
}