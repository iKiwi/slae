#include "solvers/IterationSolvers.hpp"
#include "types/Vector.hpp"
#include <gtest/gtest.h>

TEST(IterationsMethods, IterMethods1) {
  std::vector<v_c<double>> V_c = {{5, 0}, {1, 1}, {1, 0}, {3, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {2, 3};
  Vector<double, -1> x = {1, 1};

  double e = 1e-12;
  Vector<double, -1> res = Jacobi(x, A, b, e);
  Vector<double, -1> res1 = GaussSeidel(x, A, b, e);
  Vector<double, -1> res2 = SimpleIter(x, A, b, 1e-2, e);
  Vector<double, -1> res3 = SOR(x, A, b, 0.5, e);
  Vector<double, -1> res4 = SSOR(x, A, b, 0.5, e);

  ASSERT_NEAR(res[0], 3. / 14., 1e-10);
  ASSERT_NEAR(res[1], 13. / 14., 1e-10);
  ASSERT_NEAR(res1[0], 3. / 14., 1e-10);
  ASSERT_NEAR(res1[1], 13. / 14., 1e-10);
  ASSERT_NEAR(res2[0], 3. / 14., 1e-10);
  ASSERT_NEAR(res2[1], 13. / 14., 1e-10);
  ASSERT_NEAR(res3[0], 3. / 14., 1e-10);
  ASSERT_NEAR(res3[1], 13. / 14., 1e-10);
  ASSERT_NEAR(res4[0], 3. / 14., 1e-10);
  ASSERT_NEAR(res4[1], 13. / 14., 1e-10);
}

TEST(IterationsMethods, IterMethods2) {
  std::vector<v_c<double>> V_c = {{9, 0}, {1, 1}, {1, 2}, {1, 0}, {5, 1},
                                  {1, 2}, {1, 0}, {1, 1}, {7, 2}};
  std::vector<double> r = {0, 3, 6, 9};

  CSR<double> A(3, V_c, r);

  Vector<double, -1> b = {4, 3, 7};
  Vector<double, -1> x = {1, 6, 2};

  double e = 1e-12;
  Vector<double, -1> res = Jacobi(x, A, b, e);
  Vector<double, -1> res1 = GaussSeidel(x, A, b, e);
  Vector<double, -1> res2 = SimpleIter(x, A, b, 1e-2, e);
  Vector<double, -1> res3 = SOR(x, A, b, 0.5, e);
  Vector<double, -1> res4 = SSOR(x, A, b, 0.5, e);

  ASSERT_NEAR(res[0], 45. / 148., 1e-10);
  ASSERT_NEAR(res[1], 53. / 148., 1e-10);
  ASSERT_NEAR(res[2], 67. / 74., 1e-10);

  ASSERT_NEAR(res1[0], 45. / 148., 1e-10);
  ASSERT_NEAR(res1[1], 53. / 148., 1e-10);
  ASSERT_NEAR(res1[2], 67. / 74., 1e-10);

  ASSERT_NEAR(res2[0], 45. / 148., 1e-10);
  ASSERT_NEAR(res2[1], 53. / 148., 1e-10);
  ASSERT_NEAR(res2[2], 67. / 74., 1e-1);


  ASSERT_NEAR(res3[0], 45. / 148., 1e-10);
  ASSERT_NEAR(res3[1], 53. / 148., 1e-10);
  ASSERT_NEAR(res3[2], 67. / 74., 1e-1);

  ASSERT_NEAR(res4[0], 45. / 148., 1e-10);
  ASSERT_NEAR(res4[1], 53. / 148., 1e-10);
  ASSERT_NEAR(res4[2], 67. / 74., 1e-1);

}
