//
// Created by ivan on 08.04.23.
//

#include "solvers/GradientMethods.hpp"
#include "types/Vector.hpp"
#include <gtest/gtest.h>


TEST(GradientMethods, CG_1) {
  std::vector<v_c<double>> V_c = {{9, 0}, {1, 1}, {1, 2}, {1, 0}, {5, 1},
                                  {1, 2}, {1, 0}, {1, 1}, {7, 2}};
  std::vector<double> r = {0, 3, 6, 9};

  CSR<double> A(3, V_c, r);

  Vector<double, -1> b = {4, 3, 7};
  Vector<double, -1> x = {1, 6, 2};

  double e = 1e-16;

  auto res = CG(x, A, b, e);

  ASSERT_NEAR(res[0], 45. / 148., 1e-12);
  ASSERT_NEAR(res[1], 53. / 148., 1e-12);
  ASSERT_NEAR(res[2], 67. / 74., 1e-12);
}

TEST(GradientMethods, CG_2) {
  std::vector<v_c<double>> V_c = {{5, 0}, {1, 1}, {1, 0}, {3, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {2, 3};
  Vector<double, -1> x = {1, 1};

  double e = 1e-14;

  auto res = CG(x, A, b, e);

  ASSERT_NEAR(res[0], 3. / 14., e);
  ASSERT_NEAR(res[1], 13. / 14., e);
}



TEST(GradientMethods, CG_3) {
  std::vector<v_c<double>> V_c = {{M_PI, 0}, {1, 1}, {1, 0}, {2.*M_1_PI, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {M_PI, 1};
  Vector<double, -1> x = {3, 1};

  double e = 1e-12;

  auto res = CG(x, A, b, e);

  ASSERT_NEAR(res[0], 1., e);
  ASSERT_NEAR(res[1], 0., e);
}


TEST(GradientMethods, HVB_1) {
  std::vector<v_c<double>> V_c = {{9, 0}, {1, 1}, {1, 2}, {1, 0}, {5, 1},
                                  {1, 2}, {1, 0}, {1, 1}, {7, 2}};
  std::vector<double> r = {0, 3, 6, 9};

  CSR<double> A(3, V_c, r);

  Vector<double, -1> b = {4, 3, 7};
  Vector<double, -1> x = {1, 6, 2};

  double e = 1e-16;

  auto res = HeavyBall(x, A, b, e);

  ASSERT_NEAR(res[0], 45. / 148., 1e-12);
  ASSERT_NEAR(res[1], 53. / 148., 1e-12);
  ASSERT_NEAR(res[2], 67. / 74., 1e-12);
}


TEST(GradientMethods, HVB_2) {
  std::vector<v_c<double>> V_c = {{5, 0}, {1, 1}, {1, 0}, {3, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {2, 3};
  Vector<double, -1> x = {1, 1};

  double e = 1e-14;

  auto res = HeavyBall(x, A, b, e);

  ASSERT_NEAR(res[0], 3. / 14., e);
  ASSERT_NEAR(res[1], 13. / 14., e);
}

TEST(GradientMethods, HVB_3) {
  std::vector<v_c<double>> V_c = {{M_PI, 0}, {1, 1}, {1, 0}, {2.*M_1_PI, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {M_PI, 1};
  Vector<double, -1> x = {3, 1};

  double e = 1e-16;

  auto res = HeavyBall(x, A, b, e);
  ASSERT_NEAR(res[0], 1., 10*e);
  ASSERT_NEAR(res[1], 0., 10*e);
}


TEST(GradientMethods, GradDescent_1) {
  std::vector<v_c<double>> V_c = {{9, 0}, {1, 1}, {1, 2}, {1, 0}, {5, 1},
                                  {1, 2}, {1, 0}, {1, 1}, {7, 2}};
  std::vector<double> r = {0, 3, 6, 9};

  CSR<double> A(3, V_c, r);

  Vector<double, -1> b = {4, 3, 7};
  Vector<double, -1> x = {1, 6, 2};

  double e = 1e-16;

  auto res = GradientDescent(x, A, b, e);

  ASSERT_NEAR(res[0], 45. / 148., 1e-12);
  ASSERT_NEAR(res[1], 53. / 148., 1e-12);
  ASSERT_NEAR(res[2], 67. / 74., 1e-12);
}

TEST(GradientMethods, GradDescent_2) {
  std::vector<v_c<double>> V_c = {{5, 0}, {1, 1}, {1, 0}, {3, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {2, 3};
  Vector<double, -1> x = {1, 1};

  double e = 1e-14;

  auto res = GradientDescent(x, A, b, e);

  ASSERT_NEAR(res[0], 3. / 14., e);
  ASSERT_NEAR(res[1], 13. / 14., e);
}

TEST(GradientMethods, GradDescent_3) {
  std::vector<v_c<double>> V_c = {{M_PI, 0}, {1, 1}, {1, 0}, {2.*M_1_PI, 1}};
  std::vector<double> r = {0, 2, 4};

  CSR<double> A(2, V_c, r);

  Vector<double, -1> b = {M_PI, 1};
  Vector<double, -1> x = {3, 1};

  double e = 1e-16;

  auto res = GradientDescent(x, A, b, e);
  ASSERT_NEAR(res[0], 1., 10*e);
  ASSERT_NEAR(res[1], 0., 10*e);
}