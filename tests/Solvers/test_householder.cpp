#include "solvers/Householder.hpp"
#include <gtest/gtest.h>

TEST(Householder, StaticTest1) {
  DenseMatrix<double, 3, 3> A{};

  int count = 0;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 3; ++j) {
      A(i, j) = i == j ? (4 + i) * static_cast<double>(++count)
                       : static_cast<double>(++count);
    }
  }
  auto QR = HouseHolder(A);
  auto res = QR.Q * QR.R;
  ASSERT_DOUBLE_EQ(res(0, 0), A(0, 0));
  ASSERT_DOUBLE_EQ(res(1, 0), A(1, 0));
  ASSERT_DOUBLE_EQ(res(2, 0), A(2, 0));
  ASSERT_DOUBLE_EQ(res(1, 1), A(1, 1));
  ASSERT_DOUBLE_EQ(res(1, 2), A(1, 2));
  ASSERT_DOUBLE_EQ(res(2, 2), A(2, 2));
}

TEST(Householder, StaticTest2) {
  DenseMatrix<double, 6, 3> A{};

  int count = 0;
  for (int i = 0; i < 6; ++i) {
    for (int j = 0; j < 3; ++j) {
      A(i, j) = i == j ? (i + i) * static_cast<double>(++count)
                       : static_cast<double>(++count) - j + 2;
    }
  }
  auto QR = HouseHolder(A);
  auto res = QR.Q * QR.R;
  ASSERT_DOUBLE_EQ(res(0, 0), A(0, 0));
  ASSERT_DOUBLE_EQ(res(1, 0), A(1, 0));
  ASSERT_DOUBLE_EQ(res(2, 0), A(2, 0));
  ASSERT_DOUBLE_EQ(res(1, 1), A(1, 1));
  ASSERT_DOUBLE_EQ(res(1, 2), A(1, 2));
  ASSERT_DOUBLE_EQ(res(2, 2), A(2, 2));
}

TEST(Householder, StaticTest3) {
  DenseMatrix<double, 10, 5> A{};

  int count = 0;
  for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 5; ++j) {
      A(i, j) = i == j ? (i + i) * static_cast<double>(++count)
                       : static_cast<double>(++count) - j + 2 + std::cbrt(static_cast<double>(j*i));
    }
  }
  auto QR = HouseHolder(A);
  auto res = QR.Q * QR.R;
  ASSERT_DOUBLE_EQ(res(0, 0), A(0, 0));
  ASSERT_DOUBLE_EQ(res(1, 0), A(1, 0));
  ASSERT_DOUBLE_EQ(res(2, 0), A(2, 0));
  ASSERT_DOUBLE_EQ(res(1, 1), A(1, 1));
  ASSERT_DOUBLE_EQ(res(1, 2), A(1, 2));
  ASSERT_DOUBLE_EQ(res(2, 2), A(2, 2));
}
