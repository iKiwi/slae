#include "types/Vector.hpp"
#include <gtest/gtest.h>

TEST(StaticVec, init) {
  Vector<double, 3> a;
  Vector<double, 3> b{1, 2, 3};
  Vector<double, 3> c(b);
  Vector<double, 3> d = {1, 2, 3};

  ASSERT_TRUE(b[1] == 2.);
  ASSERT_TRUE(c[2] == 3.);
  ASSERT_TRUE(d[0] == 1.);
}

TEST(StaticVec, operations) {
  Vector<double, 3> a{1, 2, 3};
  Vector<double, 3> b{4, 5, 6};

  ASSERT_TRUE((a *= 2.)[1] == 4.);
  ASSERT_TRUE((a /= 2.)[1] == 2.);
  ASSERT_TRUE((a += b)[1] == 7.);
  ASSERT_TRUE((a -= b)[1] == 2.);

  ASSERT_TRUE((a * 2.)[1] == 4.);
  ASSERT_TRUE((a / 2.)[1] == 1.);
  ASSERT_TRUE((a + b)[1] == 7.);
  ASSERT_TRUE((a - b)[1] == -3.);

  ASSERT_TRUE((2. * b)[0] == 8.);
  Vector<double, 2> c{3, 4};
  ASSERT_TRUE(c.norm() == 5.);

  ASSERT_TRUE((a == Vector<double, 3>{1, 2, 3}));
}

TEST(DynamicVec, init) {
  Vector<double, -1> a;
  Vector<double, -1> b{1, 2, 3};
  Vector<double, -1> c(b);
  Vector<double, -1> d = {1, 2, 3};
  Vector<double, -1> f(4);
  f = {1., 2., 3., 4.};

  ASSERT_TRUE(b[1] == 2.);
  ASSERT_TRUE(c[2] == 3.);
  ASSERT_TRUE(d[0] == 1.);
  ASSERT_TRUE(f[3] == 4.);
}

TEST(DynamicVec, operations) {
  Vector<double, -1> a{1, 2, 3};
  Vector<double, -1> b{4, 5, 6};

  ASSERT_TRUE((a *= 2.)[1] == 4.);
  ASSERT_TRUE((a /= 2.)[1] == 2.);
  ASSERT_TRUE((a += b)[1] == 7.);
  ASSERT_TRUE((a -= b)[1] == 2.);

  ASSERT_TRUE((a * 2.)[1] == 4.);
  ASSERT_TRUE((a / 2.)[1] == 1.);
  ASSERT_TRUE((a + b)[1] == 7.);
  ASSERT_TRUE((a - b)[1] == -3.);

  ASSERT_TRUE((2. * b)[0] == 8.);
  Vector<double, -1> c{3, 4};
  ASSERT_TRUE(c.norm() == 5.);

  ASSERT_TRUE((a == Vector<double, -1>{1, 2, 3}));
}

TEST(StaticVec, dot) {
  Vector<double, 3> a1{1, 2, 3};
  Vector<double, 3> b1{-3, 2, 1};
  ASSERT_DOUBLE_EQ(a1.dot(b1), 4.);

  Vector<double, 3> a2{M_PI, 1, -2};
  Vector<double, 3> b2{2, 2, M_PI};
  ASSERT_DOUBLE_EQ(a2.dot(b2), 2.);

  Vector<double, 3> a3{10, 1, 1};
  Vector<double, 3> b3{-1, -2, 5};
  ASSERT_DOUBLE_EQ(a3.dot(b3), -7.);
}

TEST(DynamicVec, dot) {
  Vector<double, -1> a1{1, 2, 3};
  Vector<double, -1> b1{-3, 2, 1};
  ASSERT_DOUBLE_EQ(a1.dot(b1), 4.);

  Vector<double, -1> a2{M_PI, 1, -2};
  Vector<double, -1> b2{2, 2, M_PI};
  ASSERT_DOUBLE_EQ(a2.dot(b2), 2.);

  Vector<double, -1> a3{10, 1, 1};
  Vector<double, -1> b3{-1, -2, 5};
  ASSERT_DOUBLE_EQ(a3.dot(b3), -7.);


}

