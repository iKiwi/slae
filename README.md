# 4 SEM #SLAE#

## About

This project is a library for solving 
typical systems of linear equations written by iKiwi.
It includes basic types such as vector, different types of supported 
matrices and algorithms helping solve SLAE on them.

## Matrices
- Dense Matrix - Standard dense matrix realisation. All basic operations are overloaded.
- CSR - CSR Matrix realisation (Dynamic not implemented yet)
- Three Diagonal - A matrix with three non-zero major/sub-major axes.
-  

## Solvers
- ThreeDiagonalSolver - Solves SLAE for a Three Diagonal Matrix.


## Types
- Vector - A useful tool for operations on matrices.