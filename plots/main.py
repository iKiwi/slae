from matplotlib.colors import LinearSegmentedColormap
import data_residual
import data_cheb
import data_gradient
import matplotlib.pyplot as plt
import numpy as np

# ________________TASK 1________________ #
# first plot #

fig1, ax1 = plt.subplots(figsize=(20, 16))

ax1.grid(color='gray', linestyle='--', linewidth=3)
ax1.grid(which='minor', axis='both', linestyle=':', linewidth=5)

ax1.plot(range(len(data_residual.simple_iter)), np.log10(data_residual.simple_iter), linewidth=3, label='SimpleIter')
ax1.plot(range(len(data_residual.simple_opt_iter)), np.log10(data_residual.simple_opt_iter), linewidth=3,
         label='SimpleIterOpt')
ax1.plot(range(len(data_residual.ssor)), np.log10(data_residual.ssor), linewidth=3, label='SSOR')
ax1.plot(range(len(data_residual.chebyshev_acc)), np.log10(data_residual.chebyshev_acc), linewidth=3, label='Chebyshev')
ax1.plot(range(len(data_residual.conjugent_gradient)), np.log10(data_residual.conjugent_gradient), linewidth=3,
         label='CG')

ax1.set_xlabel('Iter', fontsize=25)
ax1.set_ylabel('Residual', fontsize=25)
ax1.legend(fontsize=25)
ax1.tick_params(axis='both', which='major', labelsize=25)
plt.savefig("ResidualWithIter.jpg")
# plt.show()

# second plot #

fig2, ax2 = plt.subplots(figsize=(20, 16))
ax2.grid(color='gray', linestyle='--', linewidth=3)
ax2.grid(which='minor', axis='both', linestyle=':', linewidth=5)

ax2.plot(data_cheb.dL, data_cheb.N, linewidth=3, label='Chebyshev (L_min fixed)')

ax2.set_xlabel('delta_L', fontsize=25)
ax2.set_ylabel('Iters', fontsize=25)
ax2.legend(fontsize=35)

plt.savefig("IterWithL.jpg")
# plt.show()

# ________________TASK 2________________ #
# x - min lambda
# y - max lambda

fig3, ax3 = plt.subplots(figsize=(20, 16))


def f(x, y):
    return 0.5 * (2 * x * x + 3 * y * y) - x - y


cmap = plt.get_cmap('RdPu')
cmap_inv = cmap.reversed()

x, y = np.meshgrid(np.arange(-0.01, 0.75, 0.005), np.arange(-0.01, 0.75, 0.005))
z = f(x, y)

num_levels = 30
un = np.linspace(0, 1, num_levels)
steps = un ** 4
levels = (z.max() - z.min()) * steps + z.min()

# Define the colors for the colormap
colors = [cmap_inv(i) for i in np.linspace(0, 1, num_levels)]

# Create the colormap
cmap_custom = LinearSegmentedColormap.from_list('my_colormap', colors, num_levels)

cf = ax3.contourf(x, y, z, levels=levels, cmap=cmap_custom)
ax3.plot(data_gradient.x_CG, data_gradient.y_CG, linewidth=3, label='CG')
ax3.plot(data_gradient.x_GD, data_gradient.y_GD, linewidth=3, label='GradDesc')
ax3.plot(data_gradient.x_HVB, data_gradient.y_HVB, linewidth=3, label='HeavyBall')
ax3.plot(data_gradient.x_SimpOpt, data_gradient.y_SimpOpt, linewidth=3, label='SIM_Opt')
ax3.plot(data_gradient.x_SIM, data_gradient.y_SIM, c="y", linewidth=3, label='SIM')
ax3.legend(fontsize=25)

cbar = plt.colorbar(cf)
cbar.ax.tick_params(labelsize=25)

# Increase the font size of the
plt.savefig("GradientMethods.jpg")
plt.show()
